import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lost-in-translation-finished',
  templateUrl: './lost-in-translation-finished.component.html',
  styleUrls: ['./lost-in-translation-finished.component.css']
})
export class LostInTranslationFinishedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
